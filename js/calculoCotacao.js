/**
 *
  5.   Elaborar um programa para a casa de câmbio que receba as cotações do dia e efetue a conversão de uma
  moeda para outra. O programa deverá permitir conversões entre:
                Real -> (BASE)
  Coroa Dinamarquesa -> 0,85
    Coroa Norueguesa -> 0,60
         Coroa Sueca -> 0,61
     Dólar Americano -> 5,41
   Dólar Australiano -> 3,87
     Dólar Canadense -> 4,22
                Euro -> 6,32
        Franco Suíço -> 5,89
     Libra Esterlina -> 7,38
                Iene -> 0,049

- O usuário pode selecionar o tipo de moeda de entrada e selecionar o tipo para o qual ele deseja converter.
- O programa deve permitir ao usuário que efetue tantas conversões quanto ele desejar.

            OBS: ESSA TABELA FOI ATUALIZADA NO DIA 19/08/2021
 *
 */

var coroaDinamarquesa = { nome: "Coroa Dinamarquesa", local: "da", sigla: "DKK", valor: 1.1738 };
var coroaNorueguesa = { nome: "Coroa Norueguesa", local: "no", sigla: "NOK", valor: 1.662 };
var coroaSueca = { nome: "Coroa Sueca", local: "sv-fi", sigla: "SEK", valor: 1.6221 };
var dolarAmericano = { nome: "Dólar Americano", local: "en-us", sigla: "USD", valor: 0.1846 };
var dolarAustraliano = { nome: "Dólar Australiano", local: "en-au", sigla: "AUD", valor: 0.2574 };
var dolarCanadense = { nome: "Dólar Canadense", local: "en-ca", sigla: "CAD", valor: 0.2361 };
var euro = { nome: "Euro", local: "de-DE", sigla: "EUR", valor: 0.1578 };
var francoSuico = { nome: "Franco Suíço", local: "fr-ch", sigla: "CHF", valor: 0.1693 };
var iene = { nome: "Iene", local: "ja", sigla: "JPY", valor: 20.2552 };
var libraEsterlina = { nome: "Libra Esterlina", local: "de-DE", sigla: "GBP", valor: 0.135 };
var real = { nome: "Real", local: "pt-br", sigla: "BRL", valor: 1 };

var moedas = [coroaDinamarquesa, coroaNorueguesa, coroaSueca, dolarAmericano, dolarAustraliano, dolarCanadense, euro, francoSuico, iene, libraEsterlina, real];

var selectMoedaInicial = document.querySelector("#idSelectMoeda1");
var itemSelectMoedaInicial;

var selectMoedaFinal = document.querySelector("#idSelectMoeda2");
var itemSelectMoedaFinal;

var inputValor = document.querySelector("#idInputValor");

var btRealizarConversao = document.querySelector("#idBtRealizarConversao");

var exibicaoConversao = document.querySelector("#idExibicaoConversao");

var textoMoeda1 = document.querySelector("#idTextoMoeda1");
var textoMoeda2 = document.querySelector("#idTextoMoeda2");

var textoValorDigitado = document.querySelector("#idTextoValorDigitado");
var textoValorConvertido = document.querySelector("#idTextoValorConvertido");

var moedaAnterior = "";

// Ao mudar a opção(select) da moeda inicial
selectMoedaInicial.addEventListener("change", function (event) {

    itemSelectMoedaInicial = this.value;

    console.log("Opção da moeda inicial escolhida: " + itemSelectMoedaInicial);

    if (itemSelectMoedaInicial == itemSelectMoedaFinal) {
        document.querySelector("#idSelecione2").selected = true;
        itemSelectMoedaFinal = "";
    }

    let moedaEscolhida = moedas.find(moeda => moeda.sigla === itemSelectMoedaInicial);
    alterarPlaceholderComponente(inputValor, "Digite o valor em " + moedaEscolhida.nome);


    let moedaSemelhante = document.querySelector("#id" + moedaEscolhida.sigla + "2");

    desabilitarComponente(moedaSemelhante);

    if (moedaAnterior != "") {
        habilitarComponente(moedaAnterior);
    }

    moedaAnterior = moedaSemelhante;

    habilitarComponente(selectMoedaFinal);
});

// Ao mudar a opção(select) da moeda final
selectMoedaFinal.addEventListener("change", function (event) {

    itemSelectMoedaFinal = this.value;

    console.log("Opção da moeda final escolhida: " + itemSelectMoedaFinal);

});

btRealizarConversao.addEventListener("click", function (event) {
    event.preventDefault();

    if (itemSelectMoedaInicial == "") {
        alert("Selecione a moeda inicial.");
        return;
    }

    if (itemSelectMoedaFinal == "") {
        alert("Selecione a moeda final.");
        return;
    }

    var formConversor = document.querySelector("#idFormConversor");

    let valor = formConversor.nmInputValor.value;

    if (valor == null || valor == "") {
        alert("Preencha o valor a ser convertido.");
        return;
    }

    valor = Number(valor);

    if (isNaN(valor)) {
        alert("Digite um valor numérico.");
        return;
    }

    if (valor <= 0) {
        alert("Digite um valor acima de \"0\".");
        return;
    }

    let moedaInicial = moedas.find(moeda => moeda.sigla === itemSelectMoedaInicial);
    let moedaFinal = moedas.find(moeda => moeda.sigla === itemSelectMoedaFinal);

    let conversao1 = (valor * moedaInicial.valor);
    let conversao2 = (valor * moedaFinal.valor);

    let conversaoFinal = ((conversao2 / conversao1) * valor);

    let valorFormatado = formatarEmMoeda(valor, moedaInicial.local, moedaInicial.sigla);
    let valorConvertido = formatarEmMoeda(conversaoFinal, moedaFinal.local, moedaFinal.sigla);

    console.log("Valor convertido: " + valorConvertido);

    alterarTextoComponente(textoMoeda1, moedaInicial.nome);
    alterarTextoComponente(textoMoeda2, moedaFinal.nome);
    alterarTextoComponente(textoValorDigitado, valorFormatado);
    alterarTextoComponente(textoValorConvertido, valorConvertido);
    exibirComponente(exibicaoConversao);
});

function formatarNumeroDecimal(number, qtdCasasDecimais, local) {
    return new Intl.NumberFormat(local, { minimumFractionDigits: qtdCasasDecimais }).format(number);
}

function formatarEmMoeda(number, local, sigla) {
    return new Intl.NumberFormat(local, { style: 'currency', currency: sigla }).format(number);
}

function exibirComponente(componente) {
    componente.classList.remove("visually-hidden");
}

function esconderComponente(componente) {
    componente.classList.add("visually-hidden");
}

function desabilitarComponente(componente) {
    componente.disabled = true;
}

function habilitarComponente(componente) {
    componente.disabled = false;
}

function limparConteudoComponente(componente) {
    componente.value = "";
}

function alterarTextoComponente(componente, texto) {
    componente.innerHTML = texto
}

function alterarPlaceholderComponente(componente, texto) {
    componente.placeholder = texto
}

/*   switch (itemSelectMoedaInicial) {
        case coroaDinamarquesa.sigla:
            novoValor = valor * 0.85;
            novoValor = formatarEmMoeda(novoValor, coroaDinamarquesa.local, coroaDinamarquesa.sigla);
            break;
        case coroaNorueguesa.sigla:
            novoValor = valor * 0.60;
            novoValor = formatarEmMoeda(novoValor, coroaNorueguesa.local, coroaNorueguesa.sigla);
            break;
        case coroaSueca.sigla:
            novoValor = valor * 0.61;
            novoValor = formatarEmMoeda(novoValor, coroaSueca.local, coroaSueca.sigla);
            break;
        case dolarAmericano.sigla:
            novoValor = valor * 5.41;
            novoValor = formatarEmMoeda(novoValor, dolarAmericano.local, dolarAmericano.sigla);
            break;
        case dolarAustraliano.sigla:
            novoValor = valor * 3.87;
            novoValor = formatarEmMoeda(novoValor, dolarAustraliano.local, dolarAustraliano.sigla);
            break;
        case dolarCanadense.sigla:
            novoValor = valor * 4.22;
            novoValor = formatarEmMoeda(novoValor, dolarCanadense.local, dolarCanadense.sigla);
            break;
        case euro.sigla:
            novoValor = valor * 6.32;
            novoValor = formatarEmMoeda(novoValor, euro.local, euro.sigla);
            break
        case francoSuico.sigla:
            novoValor = valor * 5.89;
            novoValor = formatarEmMoeda(novoValor, francoSuico.local, francoSuico.sigla);
            break;
        case iene.sigla:
            novoValor = valor * 0.049;
            novoValor = formatarEmMoeda(novoValor, iene.local, iene.sigla);
            break;
        case libraEsterlina.sigla:
            novoValor = valor * 7, 38;
            novoValor = formatarEmMoeda(novoValor, libraEsterlina.local, libraEsterlina.sigla);
            break;
        default:
            alert("Sigla não encontrada...");
            return;
    } */